function armarSocio(socio) {
  var medicoCabecera = tomarMedico(socio.medicocabecera);
  var historialMedico = tomarHistorialMedico(socio.documento);
  $('#nombreSocio').html('<h4>'+ socio.nombre +'</h4>');
  $('#medicoCabecera h6').append(medicoCabecera.nombre);
}

function tomarMedico(numeroMedico) {
  for(i=0; Medicos.length > 0; i++) {
    if(Medicos[i].numero === numeroMedico){
      return Medicos[i];
    }
  }
}

function tomarHistorialMedico(numeroDocumento) {
  // TODO: 
}

/*

Array
arry = [1, 2, 3, 4, 5]; // arry simple

Objeto
obj = {'propiedades':'valores', 'propiedad2':'valor'}

Array Asociativo
arryAsoc = [
  {
    'nombre': 'juan perez',
    'especialidad': 'medicina general'
  }, // Medico 1
  {
    'nombre':  'Gabriel',
    'especialidad': 'otorrino'
  } // Medico 2
]

Como cortar un objeto del array Asociativo
medico = arryAsoc[1];

Como llamar una propiedad de un objet
medico.nombre;
medico.especialidad;


Como reasignar un valor a una propiedad de un objeto
usuario.medicoCabecera = '356783';
*/
