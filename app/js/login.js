function login(rol, idUsuario, contrasena) {
  var usuario;

  if(rol === 'socio') {
    for(i=0; Socios.length > i; i++){
      if(Socios[i].documento === idUsuario) {
        if(Socios[i].clave === contrasena){
            var usuario = Socios[i];
            usuario.rol = 'socio';
  
        } else {
            console.log('contraseña erronea')
        }
      }else{
        console.log('documento no encontrado')
      }
    }
    isLoggedIn(true, usuario);
  }

  if(rol === 'medico') {
    for(i=0; Medicos.length > i; i++){
      if(Medicos[i].numero === idUsuario) {
        if(Medicos[i].clave === contrasena){

            var usuario = Medicos[i];
            usuario.rol = 'medico';
        } else {
            console.log('contraseña erronea')
        }
      }else{
        console.log('Nº profesional no encontrado')
      }
    }
    isLoggedIn(true, usuario);
  }

  console.log(usuario);
}

function isLoggedIn(estado, usuario) {
  if(estado = true) {
    $('[data-estado="nologeado"]').hide();

    if(usuario.rol === 'socio'){
        $('[data-estado="socio"]').show();
        armarSocio(usuario);
    }

    if(usuario.rol === 'medico'){
        $('[data-estado="medico"]').show();
        armarMedico(usuario);
    }
  }
}
