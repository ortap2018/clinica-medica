var Medicos = [
    {
      'numero': '123456',
      'nombre':'Juan Perez',
      'especialidad':'Medicina General',
      'clave':'123456'
    },
    {
      'numero': '123457',
      'nombre':'Juan Gonzalez',
      'especialidad':'Cardiologo',
      'clave':'123456'
    }
];

var Socios = [
    {
      'documento': '7896541',
      'nombre':'Ana Gonzalez',
      'medicocabecera': '123456',
      'clave':'123'
    }
];

var HistorialMédico = [
    {
      'actuacion': '1',
      'paciente':'7896541',
      'medico': '123456',
      'fecha': '2018-02-20',
      'motivo': 'Texto del motivo de la consulta…',
      'diagnostico': 'Texto del diagnóstico del médico….',
      'prescripción': 'texto de la prescripción del médico...',
      'imagen': 'archivo.jpg'
    },
    {
      'actuacion': '1',
      'paciente':'1234985',
      'medico': '123456',
      'fecha': '2018-02-20',
      'motivo': 'Texto del motivo de la consulta…',
      'diagnostico': 'Texto del diagnóstico del médico….',
      'prescripción': 'texto de la prescripción del médico...',
      'imagen': 'archivo.jpg'
    }

]
